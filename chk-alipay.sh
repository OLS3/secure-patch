#! /bin/bash
#------------------------------------------------------
# $Id: chk-alipay.sh,v 1.5 2013/12/15 05:34:56 ols3 Exp $
# Target: alipay plugin.
# Written by OLS3 (ols3@lxer.idv.tw)
# WebSite: http://freesf.tw
#------------------------------------------------------
shopt -s -o nounset

# alipay plugin
Targetf="aliedit.sh"
# Temp dir
Tmpdir="./tmp"

# check
[ -f "./$Targetf" ] || { echo "Usage: $0 $Targetf" ; exit 1; } 

# archive mark line number + 1
# method 1 
poz=$[$(grep -ans '^__ARCHIVE_BELOW__$' $Targetf | awk -F: '{print $1}')+1]
# method 2
poz=$(awk '/^__ARCHIVE_BELOW__$/{print NR+1}' $Targetf)
# doit
mkdir -p $Tmpdir && tail -n +$poz aliedit.sh | tar xvzf - -C $Tmpdir
#
echo
echo "Please check $Tmpdir"

# usage & output examples 
: <<EOF
root@ob2d:~# chmod +x chk-alipay.sh
root@ob2d:~# ./chk-alipay.sh
install.sh
lib/
lib/libaliedit32.so
lib/libaliedit64.so
README

Please check ./tmp
EOF
