#! /bin/bash
#------------------------------------------------------
# $Id: dt-lworm.sh,v 1.6 2013/11/30 13:14:46 ols3 Exp $
# Target: detect Linux.Darlloz worm.
# Written by OLS3 (ols3@lxer.idv.tw)
# WebSite: http://freesf.tw
#------------------------------------------------------
shopt -s -o nounset

# log file
LOGF="/var/log/apache2/access.log"
# pattern
RegEx="/cgi-bin/php*"
# temp file
Tmpf=$(mktemp)
cnt=
ip=
dn=
# doit
grep "$RegEx" $LOGF | awk '{print $1}' | sort | uniq -c > $Tmpf
while read cnt ip
do
	# comment out dn, if u dont want to get its domain name.
	dn=$(host $ip | awk '{print $5}')
	echo "$cnt $ip $dn"
done < $Tmpf

# usage & output examples 
: <<EOF
root@ob2d:~# chmod +x dt-lworm.sh
root@ob2d:~# ./dt-lworm.sh 
1 120.116.18.253 3(NXDOMAIN)
2 192.151.144.234 3(NXDOMAIN)
5 212.174.183.46 no
5 218.80.198.230 3(NXDOMAIN)
5 94.102.49.37 craigie.playimpressive.com.
EOF
